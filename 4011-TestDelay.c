/* \file 4011-TestDelay.c
 *
 * \brief  Measure delay times
 *
 * The point of this simple blink an LED is to establish a delay
 * function based on a C for loop rather than assembler. Note that
 * that a limitation is that the function cannot be called for
 * multiple seconds.  For example
 *
 *   Delay(3*Cnt_1s);
 *
 * would result in an argument in the neighborhood of 81000, too large
 * for an unsigned int.
 *
 *    ************************************************************
 *    *                                                          *
 *    *                 W A R N I N G                            *
 *    *                                                          *
 *    *   Only gives expected results when optimization is off   *
 *    *                                                          *
 *    ************************************************************
 *
 * Author: jjmcd
 *
 * Created on September 6, 2012, 6:51 AM
 * Updated to scale outer counter based on clock September 11, 2012
 */
#include <p30Fxxxx.h>

// Configuration fuses
_FOSC (XT & PRI)                        // 7.3728 rock /4 = 1.8432 MIPS
_FWDT (WDT_OFF)                         // Watchdog timer off
_FBORPOR (PWRT_16 & PBOR_OFF & MCLR_EN) // Brownout off, powerup 16ms
_FGS (GWRP_OFF & CODE_PROT_OFF)         // No code protection

//#define Fcy 29491200                    // Instruction frequency
#define Fcy 7372800/4
// Counts for various delays
#define Cnt_1ms     28
#define Cnt_2ms     55
#define Cnt_5ms    137
#define Cnt_15ms   410
#define Cnt_1s   27332
// Calculate outer loop counter value
#define LOOPCNT (Fcy / 201404)

#define WAITFOR Cnt_1s/2

// Delay for a specified count.
void Delay (unsigned int n)
{
  unsigned int i, j;

  for ( j=0; j<LOOPCNT; j++ )
    for ( i=0; i<n; i++)
      ;
}
/*           Measured Frequencies
 *           --------------------
 *                      PLL16      PLL4       XT
 *   Cnt   Expected   29.4912    7.3728    1.8432
 *   1ms     500     426.1287  431.3611   428.6567
 *   2ms     250     222.9971  225.9063   225.1626
 *   5ms     100      91.1037   92.3375    92.2132
 *  15ms      33      30.6837   31.1062    31.0922
 *
*/

int main (void)
{
  TRISD &= 0xfff9;          // PORTD1 output

  while (1)
    {
      _LATD1 = 0;           // LED on
      _LATD2 = 1;
      Delay( WAITFOR );
      _LATD1 = 1;           // LED off
      _LATD2 = 0;
      Delay( WAITFOR );
    }
}
